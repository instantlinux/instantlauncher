#!/bin/sh

if [ `wmctrl -l | grep -c 'InstantLauncher'` != 0 ]; then
    wmctrl -a 'InstantLauncher'
else
    /opt/instantlauncher/instantlauncher
fi
