# Installation instructions

System dependencies:
* NodeJS
* wmctrl

At first you have to install the dependencies:

```sh
npm install
```

Then you have to rebuild a module:

```sh
npm run rebuild
```

After that you can build the ts-source-files:

```sh
npm run build
```

Now you can package the launcher:

```sh
npm run package
```

You can install instantlauncher on your system (recommended folder for now is /opt/instantlauncher):

```sh
cp -Rav dist/instantlauncher-linux-x64 /opt/instantlauncher
```

We include a simple script to launch instantlauncher. You can copy this script into your /usr/bin-folder:

```sh
cp instantlauncher.sh /usr/bin/instantlauncher
chmod +x /usr/bin/instantlauncher
```

You should also create a system-wide folder for widgets:

```sh
mkdir -p /usr/share/instantdesktop/instantlauncher/widgets
```