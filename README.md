# InstantLauncher
> Launcher for our Desktop Environment.

InstantLauncher is a modular, web-based launcher for `InstantDesktop`. It is written using electron and nodejs.

![Screenshot](images/screenshot.png?raw=true "Screenshot")

## Dependencies

* NodeJS

## Testing

```sh
npm test
```

If another instance is running, this command executes the launcher while killing already running instances.

```sh
npm start
```

If another instance is running, this command brings that window to front again.

## Customizing your launcher

When clicking on the `Settings`-Icon in the upper right corner, another view will open up. There you can move or resize already existing widgets, or you can add Widgets by clicking the `+` symbol. You can remove widgets by middle-clicking on it.

![Settings](images/settings.gif?raw=true "Settings")

## Release History

* Initial Commit

## Meta

Hannes Schulze – [guidedlinux.org](https://www.guidedlinux.org/) – projects@guidedlinux.org

Distributed under the GPL-3.0 license. See ``LICENSE`` for more information.

[https://github.com/guidedlinux/](https://github.com/guidedlinux/)

## Contributing

1. Fork it (<https://github.com/InstantCodee/instantLauncher.git>)
1. Create your feature branch (`git checkout -b feature/fooBar`)
1. Commit your changes (`git commit -am 'Add some fooBar'`)
1. Push to the branch (`git push origin feature/fooBar`)
1. Create a new Pull Request
