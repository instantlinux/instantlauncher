var x11 = require('x11');

var X;

var getWindowName = function(wid) {
    return new Promise(function(resolve, reject) {
        X.InternAtom(false, '_NET_WM_NAME', function(wmNameErr, wmNameAtom) {
            X.InternAtom(false, 'UTF8_STRING', function(utf8err, utf8Atom) {
                X.GetProperty(0, wid, wmNameAtom, utf8Atom, 0, 10000000, function(err, nameProp) {
                    if (err) {
                        reject(err);
                    }
                    resolve(nameProp.data.toString());
                });
            });
        });
    });
}

module.exports.getWindowID = function(name) {
    return new Promise(function(resolve, reject) {
        x11.createClient(function(err, display) {
            X = display.client;
            var root = display.screen[0].root;
            X.QueryTree(root, function(err, tree) {
                tree.children.map(function(id) {
                    let prop = getWindowName(id).then(function(n) {
                        if (n === name) {
                            resolve(id);
                        }
                    });
                });
            });
        });
    });
}