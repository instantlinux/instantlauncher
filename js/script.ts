import { remote } from 'electron';
import * as path from 'path';
import * as fs from 'fs';
import mkdirp from 'mkdirp';
import bettersqlite3 from 'better-sqlite3';
const app: Electron.App = remote.app;

let divMainArea: HTMLElement = document.getElementById("mainarea")!;
let divAddHotcorner: HTMLElement = document.getElementById("addhotcorner")!;

declare var interact: any;

// Search paths for widgets
const localsearchpath = path.join(app.getPath('home'), '.instantlauncher/widgets');
const searchpaths = [
    '/usr/share/instantdesktop/instantlauncher/widgets',
    localsearchpath
];
var refreshing = true;

// Widgets selected by the user
var widgets: Array<LauncherWidget> = [];

interface LauncherWidget {
    widgetName: string;
    widgetX: number;
    widgetY: number;
    widgetWidth: number;
    widgetHeight: number;
    domid: string;
    widgetfolder: string;
}

function loadevent() {
    // Create local widget search path if it doesn't exist
    if (!fs.existsSync(localsearchpath)) {
        mkdirp(localsearchpath, function (err) {
            if (err) console.log(err);
            else readdb();
        });
    } else { readdb(); }
    document.body.onkeyup = function(event) {
        if (event.keyCode == 27) {
            remote.getCurrentWindow().minimize();
        }
    }
}

function readdb() {
    // Open SQLite database with selected widgets.
    var configpath = path.join(app.getPath('appData'), 'instantlauncher.db');

    var db = new bettersqlite3(configpath);
    db.prepare("CREATE TABLE IF NOT EXISTS widgets (name, xpos, ypos, width, height)").run();

    var result = db.prepare("SELECT * FROM widgets").all();
    result.forEach(row => {
       widgets.push({
           widgetName: row.name,
           widgetX: parseInt(row.xpos),
           widgetY: parseInt(row.ypos),
           widgetWidth: parseInt(row.width),
           widgetHeight: parseInt(row.height),
           domid: '',
           widgetfolder: ''
       });
    });

    db.close();
    drawwidgets();
}

async function drawwidgets() {
    var i = 0;
    widgets.forEach(widget => {
        var widgetfolder = "";
        searchpaths.forEach(searchpath => {
            if (fs.existsSync(path.join(searchpath, widget.widgetName, "package.json"))) { // Script path for current widget
                widgetfolder = path.join(searchpath, widget.widgetName);
            }
        });
        if (widgetfolder != "") { // If the widget's script exists in one search path...
            widget.widgetfolder = widgetfolder;
            import(widgetfolder).then(async widgetGenerator => {
                i++;
                widget.domid = widget.widgetName + i;
                var domelement = document.createElement("div"); // ... create the widget with the current id.
                domelement.id = widget.domid;
                
                domelement.style.width = (widget.widgetWidth * 2.5) + "%";
                domelement.style.height = (widget.widgetHeight * 2.5) + "%";

                domelement.style.top = (widget.widgetY * 2.5) + "%";
                domelement.style.left = (widget.widgetX * 2.5) + "%";

                domelement.style.position = "absolute";
                domelement.style.background = "rgba(45, 45, 51, 0.6)";
                domelement.style.boxShadow = "2px 2px 10px rgba(0, 0, 0, 0.25)";

                domelement.appendChild(await widgetGenerator["default"](widgetfolder, function() {
                    remote.getCurrentWindow().minimize();
                })); // Append the script's result in the current widget's container.
                divMainArea.appendChild(domelement);
            }).catch(error => {
                console.error("Cannot load module! " + widget.widgetName);
                console.log(error);
            });
        } else { console.log("Widget not found: " + widget.widgetName); }
    });
}

var availableWidgets: Array<string> = [];

function getAvailableWidgets() {
    availableWidgets = [];
    divPopup.innerHTML = "";
    searchpaths.forEach(searchpath => {
        fs.readdirSync(searchpath).forEach(searchitem => {
            if (fs.existsSync(path.join(searchpath, searchitem, "package.json")) && !availableWidgets.includes(searchitem)) {
                availableWidgets.push(searchitem);
            }
        });
    });

    drawItems();
}

function drawItems() {
    availableWidgets.forEach(widget => {
        var itemContainer = document.createElement("div");

        itemContainer.style.background   = "rgba(45, 45, 51, 0.6)";
        itemContainer.style.boxShadow    = "2px 2px 10px rgba(0, 0, 0, 0.25)";
        itemContainer.style.border       = "2px solid #fafafa";
        itemContainer.style.borderRadius = "5px";
        itemContainer.style.width        = "300px";
        itemContainer.style.height       = "150px";
        itemContainer.style.display      = "inline-block";
        itemContainer.style.margin       = "10px";
        itemContainer.style.cursor       = "pointer";

        var itemText = document.createElement("p");
        itemText.innerHTML = widget;

        itemText.style.fontFamily     = "Raleway";
        itemText.style.fontWeight     = "700";
        itemText.style.color          = "#fafafa";
        itemText.style.letterSpacing  = "2px";
        itemText.style.textAlign      = "center";
        itemText.style.margin         = "0";
        itemText.style.marginTop      = "75px";
        itemText.style.transform      = "translate(0, -50%)";
        itemText.style.display        = "flex";
        itemText.style.justifyContent = "center";

        itemContainer.appendChild(itemText);

        divPopup.appendChild(itemContainer);

        itemContainer.onclick = function() {
            divMainArea.innerHTML = "";
            widgetssettings.push({
                widgetName: widget,
                widgetX: 0,
                widgetY: 0,
                widgetWidth: 3,
                widgetHeight: 1,
                domid: ''
            });
            drawwidgetssettings();
        };
    });
}

var settingsActive = false;

var widgetssettings: Array<widgetSetting> = [];

interface widgetSetting {
    widgetName: string;
    widgetX: number;
    widgetY: number;
    widgetWidth: number;
    widgetHeight: number;
    domid: string;
    deleted?: boolean;
}

function showSettings() {
    if (settingsActive) {
        applychanges();
        divAddHotcorner.style.display = "none";
        widgets = [];
        refreshing = true;
        divMainArea.innerHTML = "";
        readdb();
    } else {
        widgetssettings = []
        divAddHotcorner.style.display = "block";
        divMainArea.innerHTML = "";
        refreshing = false;
        readdbsettings();
    }

    settingsActive = !settingsActive;
}

function applychanges() {
    var db = new bettersqlite3(path.join(app.getPath('appData'), 'instantlauncher.db'));

    db.prepare("DROP TABLE IF EXISTS `widgets`").run();
    db.prepare("CREATE TABLE IF NOT EXISTS widgets (name, xpos, ypos, width, height)").run();

    widgetssettings.forEach(widget => {
        if (!widget.deleted) {
            let widgetDomElement: HTMLElement = document.getElementById(widget.domid)!;
            var xOffset = Math.round((parseFloat(widgetDomElement.getAttribute('data-x')!) || 0) / document.documentElement.clientWidth * 40);
            var yOffset = Math.round((parseFloat(widgetDomElement.getAttribute('data-y')!) || 0) / document.documentElement.clientHeight * 40);
            var newX: number = widget.widgetX + xOffset;
            var newY: number =  widget.widgetY + yOffset;
            var newWidth = Math.round(widgetDomElement.clientWidth / document.documentElement.clientWidth * 40);
            var newHeight = Math.round(widgetDomElement.clientHeight / document.documentElement.clientHeight * 40);

            db.prepare("INSERT INTO `widgets` VALUES('" + widget.widgetName + "', '" + newX + "', '" + newY + "', '" + newWidth + "', '" + newHeight + "')").run();

            interact("#" + widget.domid).unset();
        }
    });
    db.close();
}

function readdbsettings() {
    // Open SQLite database with selected widgets.
    var configpath = path.join(app.getPath('appData'), 'instantlauncher.db');

    var db = new bettersqlite3(configpath);
    db.prepare("CREATE TABLE IF NOT EXISTS widgets (name, xpos, ypos, width, height)").run();

    var result = db.prepare("SELECT * FROM widgets").all();
    result.forEach(row => {
        widgetssettings.push({
            widgetName: row.name,
            widgetX: parseInt(row.xpos),
            widgetY: parseInt(row.ypos),
            widgetWidth: parseInt(row.width),
            widgetHeight: parseInt(row.height),
            domid: '',
            deleted: false
       });
    });

    db.close();
    drawwidgetssettings();
}

function drawwidgetssettings() {
    var i = 0;
    widgetssettings.forEach(widget => {
        i++;
        widget.domid = widget.widgetName + i;
        let domelement: HTMLElement = document.createElement("div"); // ... create the widget with the current id.
        domelement.id = widget.domid;

        domelement.style.width = (widget.widgetWidth * 2.5) + "%";
        domelement.style.height = (widget.widgetHeight * 2.5) + "%";

        domelement.style.top = (widget.widgetY * 2.5) + "%";
        domelement.style.left = (widget.widgetX * 2.5) + "%";

        domelement.style.position = "absolute";
        domelement.style.background = "rgba(45, 45, 51, 0.6)";
        domelement.style.boxShadow = "2px 2px 10px rgba(0, 0, 0, 0.25)";
        domelement.style.border = "2px solid #fafafa";
        domelement.style.borderRadius = "5px";

        var texttag = document.createElement("p");
        texttag.innerHTML = widget.widgetName;
        texttag.style.fontFamily    = "Raleway";
        texttag.style.fontWeight    = "700";
        texttag.style.color         = "#fafafa";
        texttag.style.letterSpacing = "2px";
        texttag.style.textAlign     = "center";
        texttag.style.margin        = "0";
        texttag.style.position      = "absolute";
        texttag.style.top           = "50%";
        texttag.style.left          = "0";
        texttag.style.right         = "0";
        texttag.style.transform     = "translate(0, -50%)";

        domelement.appendChild(texttag);
        domelement.onmousedown = function(event: any) { 
            if (event.which == 2) {
                event.preventDefault();
                widget.deleted = true;
                let widgetDomElement: HTMLElement = document.getElementById(widget.domid)!;
                widgetDomElement.remove();
            }
        };
        divMainArea.appendChild(domelement); // Add the container to our DOM.
        makeinteractive("#" + widget.domid);
    });
}

function makeinteractive(id: string) {
    interact(id)
        .draggable({
            snap: {
                targets: [
                    interact.createSnapGrid({ x: Math.round(document.documentElement.clientWidth / 40), y: Math.round(document.documentElement.clientHeight / 40) })
                ],
                range: Infinity,
                relativePoints: [{ x: 0, y: 0 }]
            },
            onmove: (window as any).dragMoveListener,
            restrict: {
                restriction: 'parent',
                elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
            },
        })
        .resizable({
            snap: {
                targets: [
                    interact.createSnapGrid({ x: Math.round(document.documentElement.clientWidth / 40), y: Math.round(document.documentElement.clientHeight / 40) })
                ],
                range: Infinity,
                relativePoints: [{ x: 0, y: 0 }]
            },
            // resize from all edges and corners
            edges: { left: true, right: true, bottom: true, top: true },

            // keep the edges inside the parent
            restrictEdges: {
                outer: 'parent',
                endOnly: true,
            },

            // minimum size
            restrictSize: {
                min: { width: 100, height: 50 },
            },

            inertia: true,
        })
        .on('resizemove', function (event: any) {
            var target = event.target,
                x = (parseFloat(target.getAttribute('data-x')) || 0),
                y = (parseFloat(target.getAttribute('data-y')) || 0);

            // update the element's style
            target.style.width = event.rect.width + 'px';
            target.style.height = event.rect.height + 'px';

            // translate when resizing from top or left edges
            x += event.deltaRect.left;
            y += event.deltaRect.top;

            target.style.webkitTransform = target.style.transform =
                'translate(' + x + 'px,' + y + 'px)';

            target.setAttribute('data-x', x);
            target.setAttribute('data-y', y);
        });
}

function dragMoveListener(event: any) {
    var target = event.target,
        // keep the dragged position in the data-x/data-y attributes
        x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
        y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

    // translate the element
    target.style.webkitTransform =
        target.style.transform =
        'translate(' + x + 'px, ' + y + 'px)';

    // update the posiion attributes
    target.setAttribute('data-x', x);
    target.setAttribute('data-y', y);
}

(window as any).dragMoveListener = dragMoveListener;

const divScreenOverlay: HTMLElement = document.getElementById("screenoverlay")!;
const divPopup: HTMLElement = document.getElementById("popup")!;

function showpopup() {
    getAvailableWidgets();
    divScreenOverlay.style.display = "block";
    divPopup.style.display = "block";
}

function hidepopup() {
    divPopup.className = "animateout";
    divScreenOverlay.className = "animateout";

    setTimeout(function() { // Wait until the animation is completed.
        divPopup.className = "";
        divScreenOverlay.className = "";
        divScreenOverlay.style.display = "none";
        divPopup.style.display = "none";
    }, 450);
}
