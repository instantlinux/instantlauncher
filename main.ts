import * as path from 'path';
import * as url from 'url';
import {app, BrowserWindow, protocol} from 'electron';
import * as gala_blur from 'gala-blur-implementation';
const blurutils = require('./js/blurUtil');


let win: Electron.BrowserWindow;

function createWindow() {
    protocol.unregisterProtocol('', () => { // Needed for creating a transparent window (https://github.com/electron/electron/issues/2170)
        win = new BrowserWindow({ // Create a transparent window
            width: 800,
            height: 600,
            transparent: true,
            frame: false,
            title: "InstantLauncher"
        });

        win.maximize();
        win.setSkipTaskbar(true); // Don't display the launcher in the Taskbar

        blurutils.getWindowID("InstantLauncher").then(function(wid: number) {
            gala_blur.BlurWindow(wid.toString());
            win.loadURL(url.format({
                pathname: path.join(__dirname, 'html/index.html'),
                protocol: 'file:',
                slashes: true
            }));
    
            win.on('blur', () => {
                win.minimize();
            });
    
            win.on('closed', () => {
                win = null!;
            });
        });
    });
}

app.disableHardwareAcceleration();
app.commandLine.appendSwitch('enable-transparent-visuals');
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    app.quit();
});

app.on('activate', () => {
    if (win === null) {
        createWindow();
    }
});